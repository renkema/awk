 
#include <stdio.h>
#include <errno.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "gawkapi.h"

int plugin_is_GPL_compatible;
static const gawk_api_t *api;
static awk_ext_id_t ext_id;
static const char *ext_version = NULL;
static awk_bool_t (*init_func)(void) = NULL;

static awk_value_t *
do_chr(int nargs, awk_value_t *result, struct awk_ext_func *unused) {
    awk_value_t ord;
    if (!get_argument(0, AWK_NUMBER, &ord)) {
        nonfatal(ext_id, "chr: not a number");
        return make_null_string(result);
    }
    char chr[5];
    sprintf(chr, "%lc", (int)ord.num_value);
    return make_const_string(chr, strlen(chr), result);
}

static awk_value_t *
do_ord(int nargs, awk_value_t *result, struct awk_ext_func *unused) {
    awk_value_t str_param;
    if (!get_argument(0, AWK_STRING, &str_param)) {
        warning(ext_id, "ord: not a string");
        return make_number(0xFFFC, result);
    }
    unsigned char* chr = (unsigned char*)str_param.str_value.str;
    if (chr[0] < 0x80) {
        return make_number(chr[0], result);
    }
    int rv=0, i=0, extra_bytes=0;
    if (chr[0] >> 5 == 0x06) {
        extra_bytes = 1;
        rv = chr[0] & 0x1F;
    } else if (chr[0] >> 4 == 0x0E) {
        extra_bytes = 2;
        rv = chr[0] & 0x0F;
    } else if (chr[0] >> 3 == 0x1E) {
        extra_bytes = 3;
        rv = chr[0] & 0x07;
    } else {
        warning(ext_id, "ord: invalid leading byte");
        return make_number(0xFFFD, result);
    }
    while ((chr[++i] & 0xC0) == 0x80) {
        rv = (rv << 6) + (chr[i] & 0x3F);
    }
    if (i-1 == extra_bytes) {
        return make_number(rv, result);
    }
    warning(ext_id, "ord: invalid byte sequence");
    return make_number(0xFFFD, result);
}
 
static awk_value_t *
do_bytes(int nargs, awk_value_t *result, struct awk_ext_func *unused) {
    awk_value_t arr_param, str_param;
    if (!get_argument(0, AWK_ARRAY, &arr_param)) {
        nonfatal(ext_id, "bytes: first argument not an array");
        return make_number(0, result);
    }
    if (!get_argument(1, AWK_STRING, &str_param)) {
        nonfatal(ext_id, "bytes: second argument not an string");
        return make_number(0, result);
    }
    awk_array_t arr = arr_param.array_cookie;
    clear_array(arr);
    unsigned char* str = (unsigned char*) str_param.str_value.str;
    awk_value_t ind, val;
    int i=0;
    while (i < str_param.str_value.len) {
        make_number(i+1, &ind);
        make_number(str[i++], &val);
        set_array_element(arr, &ind, &val);
    }
    return make_number(i, result);
}

static awk_ext_func_t func_table[] = {
    { "chr", do_chr, 1, 1 },
    { "ord", do_ord, 1, 1 },
    { "bytes", do_bytes, 2, 2 },
};

dl_load_func(func_table, uordchr, "awk");


