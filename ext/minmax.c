 
#include <stdio.h>
#include <errno.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "gawkapi.h"
#include <math.h>
#include <stdbool.h>

int plugin_is_GPL_compatible;
static const gawk_api_t *api;
static awk_ext_id_t ext_id;
static const char *ext_version = NULL;
static awk_bool_t (*init_func)(void) = NULL;

// C programming tip: write macros instead of functions wherever possible

#define make_multi_func(NAME, FUNC) \
static awk_value_t * \
NAME(int nargs, awk_value_t *result, struct awk_ext_func *unused) { \
    awk_value_t n; \
    double m = 0; \
    bool first = true; \
    for (int i=0; i<nargs; i++) { \
        get_argument(i, AWK_UNDEFINED, &n); \
        if (n.val_type == AWK_STRNUM) \
            get_argument(i, AWK_NUMBER, &n); \
        else if (n.val_type != AWK_NUMBER) { \
            nonfatal(ext_id, "argument not a number"); \
            continue; \
        } \
        if (first) { \
            first = false; \
            m = n.num_value; \
        } else { \
            m = FUNC(m, n.num_value); \
    }   } \
    if (first) fatal(ext_id, "no numerical arguments"); \
    return make_number(m, result); \
}

make_multi_func(do_min, fmin)
make_multi_func(do_max, fmax)

static awk_ext_func_t func_table[] = {
    { "min", do_min, 0, 2 },
    { "max", do_max, 0, 2 },
};

dl_load_func(func_table, minmax, "awk");


