
# SUMMARY

A collection of libraries, extensions and tools for (g)awk.

# CONTENTS

* [Programs](#programs)
* [Extensions](#extensions)
  - [Installation](#installation)
* [Libraries](#libraries)
  - [Installation](#installation-1)
* [Copying](#copying)


# PROGRAMS

The `bin/` directory contains three small programs that together cover a large 
part of my use of grep(1) and sed(1). They are meant to be used as part of 
longer pipelines:

| Program                | Replaces                                          |
|:-----------------------|:--------------------------------------------------|
| `incl pat1 pat2 ...`   | `grep "pat1\|pat2\|..."`                          |
| `excl pat1 pat2 ...`   | `grep -v pat1 \| grep -v pat2 ...`                |
| `sub pat1 repl1 ...`   | `sed s/pat1/repl1/g;s/pat2/repl1/g;...'`          |

A fourth program, `sample`, implements a simple reservoir sampling algorithm. 
Call it with the sample size as e.g. `sample n=100`.

# EXTENSIONS

The `src/` directory contains two extensions for gawk, written in c, which must 
be `@load`ed instead of `@include`d. The first, `minmax` is a toy example and 
provides multi-variable `min()` and `max()` functions that would be impossible 
to define in awk itself.

The second, `uordchr`, is a multi-byte version of the (8-bit) `ordchr` awk 
library and provides the following functions:


| Function          | Description                                            |
|:------------------|:-------------------------------------------------------|
| `ord(str)`        | Returns the code point of the first character in `str` |
| `chr(num)`        | Returns the character at the code point `num`          |
| `bytes(arr, str)` | Fills the array `arr` with the byte contents of `str`  |

**Note:** the `ord()` function assumes the encoding is utf-8 and returns 
nonsense otherwise. The two other functions might work with other multi-byte 
encodings, but this is untested.


## Installation

For compilation of the extensions, a makefile is included. After running 
`make`, proceed with the installation by moving (or linking) the resulting 
shared object (.so) files into your `$AWKLIBPATH`.
If you have no `$AWKLIBPATH` set, you can query its built-in value with

    gawk 'BEGIN{print ENVIRON["AWKLIBPATH"]}'


# LIBRARIES

The `lib/` directory contains the following ordinary library files with the 
following functions:

* In `numbers.awk`
  - `min2(a,b)`, `max2(a,b)`, `min3(a,b,c)` and `max3(a,b,c)`
  - `abs(n)`, `sign(n)`, `floor(n)` and `ceil(n)`
  - `maxind(arr)`, returning the largest index
  - `sum(arr)`, `mean(arr)`, `stdev(arr)` and `variance(arr)`
* In `strings.awk`
  - `join(arr, sep)` joins an array of strings
  - `skipjoin(arr, sep)` joins an array of strings, skipping the empty string
  - **Note:** the arrays passed to `join()` and `skipjoin()` may start at 
    either `0` or `1`, but from there on the first missing element stops the 
    operation.
  - `zipjoin(arr, seps)` joins zips an array of strings with an array of 
    separators; it is the inverse of `split()` and `patsplit()`.
  - `regex_escape(str)` escapes all special characters in `str`, making it fit 
    for inclusion in a regex pattern constructed on-the-fly.
* In `stacks.awk`
  - `push(arr, val)` and `pop(arr)`
  - `dup(arr)`, `swp(arr)` and `rot(arr)`
  - **Note:** arrays used with these functions may not contain values at any 
    index besides `0...n` or `1...n`, and all intermediate indices between `0` 
    (or `1`) and the topmost element `n` must be present.
* In `sets.awk`
  - `elem(arr, val)` assigns an element to a set (`arr[val]` tests membership)
  - `union(r, a, b)`, `intersection(r, a, b)` and `exclusion(r, a, b)` assign 
    the result of the operation to the set `r`.
  - `unite(a, b)`, `intersect(a, b)` and `exclude(a, b)` apply the operation to 
    the set `a`
* In `levenshtein.awk`
  - `levenshtein(a,b)` calculates the levenshtein distance between two strings.

Furthermore, the `recparse.awk` library allows the parsing of recfile 
databases. Please consult the top of this file for the proper way of using it.


## Installation

Move (or link) the contents of `lib/` into a directory in your `$AWKPATH`.
If you have no `$AWKPATH` set, you can query its built-in value with

    gawk 'BEGIN{print ENVIRON["AWKPATH"]}'


# COPYING

All programs are in the public domain.


