
function min2(a,b) { return b<a ? b : a }
function max2(a,b) { return b>a ? b : a }

function min3(a,b,c) { return b<a ? (c<b?c:b) : (c<a?c:a) }
function max3(a,b,c) { return b>a ? (c>b?c:b) : (c>a?c:a) }

function maxind(arr,    i, r) {
    for (i in arr) r = max(r, i)
    return r }

function abs(n)  { return n<0 ? -n : n }
function sign(n) { return n<0 ? -1 : 1 }

function floor(n,   r) { r = int(n); return n<r ? r : r-1 }
function ceil (n,   r) { r = int(n); return n>r ? r : r+1 }

function sum(a,   i, t) { for (i in a) t+=i; return t }

function mean(arr) { return sum(arr) / length(arr) }
function stddev(arr) { return sqrt(variance(arr)) }
function variance(arr,     i, n, t, t2) {
    n = length(a)
    if (n < 2) return 0
    for (i in a) { t += i; t2 += i*i }
    return (t2 - t*t/n) / (n-1) }
