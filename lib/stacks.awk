
# find the last index in the row [0|1] ... n
# allows for a start at either 0 or 1
function lastind(arr) {
    return length(arr) - (0 in arr ? 1 : 0) }

function push(arr, val) {
    arr[lastind(arr)+1] = val }

function pop(arr,   i, r) {
    i = lastind(arr)
    r = arr[i]; delete arr[i]
    return r }

function dup(arr,   i) {
    i = lastind(arr)
    arr[i+1] = arr[i] }

function swp(arr,   i, x) {
    i = lastind(arr)
    if (i<2) return
    x = arr[i]
    arr[i] = arr[i-1]
    arr[i-1] = x }

function rot(arr,   i, x) {
    i = lastind(arr)
    if (i<3) return
    x = arr[i]
    arr[i] = arr[i-2]
    arr[i-2] = arr[i-1]
    arr[i-1] = x }

