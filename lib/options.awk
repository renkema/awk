
# USAGE
#
#   @include "options"
#
#   BEGIN {
#
#     usage = "[--common] [--options]"
#
#     add_option("long-option")
#     add_option("with-abbrev", "w")
#     add_option("in-help-text", 0, "description")
#     add_option("argument-required", 0, 0, 1)
#     add_option("named-argument", 0, "for help text", "value")
#
#     options["some-option"] = "default value"
#
#     # The following line is optional. If omitted, options will be processed 
#     # incrementally, in BEGINFILE.
#
#     process_options()
#
#   }
#
# REMARKS
#
# Do run your script with -E instead of -f, so that gawk’s own option 
# processing will not interfere.
#
# As usual, -- stops option processing.
#
# A --help option will be constructed automatically from the provided option 
# descriptions, if not already present.
#
# The following are equivalent:
#   --no-option
#   --option=0
#   --option=no
#   --option=false
#
# If an argument is required, these are equivalent:
#   --option=value
#   --option value
#
# Multiple short options can be given together (as -abc instead of -a -b -c), 
# but only for the last one may an argument be required.


BEGIN { delete options
    help_text = usage = option_maxlength = option_errors = 0
    max_option_maxlength = 20
}

function add_option(long, short, description, arg_required,    i, o) {
    long_options[long] = arg_required
    if (short) short_options[short] = long
    if (description) {
        i = length(option_descriptions)+1
        o = (short ? "-"short", " : "") "--" long
        if (arg_required) o += "=" arg_required == 1 ? "..." : arg_required
        option_descriptions[i][1] = o
        option_descriptions[i][2] = description
        if (length(o) > option_maxlength)
            option_maxlength = length(o)
}   }

BEGINFILE {
    if (processing_options) {
        provide_help_option()
        if (process_option(ARGIND)) {
            may_show_help()
            nextfile
}   }   }

function process_options(   i) {
    provide_help_option()
    for (i=1;i<ARGC;i++) {
        if (process_option(i)) delete ARGV[i]
        if (!processing_options) break
    }
    may_show_help()
    processing_options = false
}

function process_option(idx,    a,m,i) {
    a = ARGV[idx]
    if (a == "--")
        # stop option processing
        processing_options = 0
    else if (match(a, /^--no-([^=]+)$/, m))
        # --no-option
        set_long_option(m[1], 0)
    else if (match(a, /^--([^=]+)(=(.*))?/, m)) {
        # --long-option=value
        o = m[1];
        if (2 in m) {
            a = m[2]
            if (a ~ /^(false|no)$/) a = 0
        } else if (long_options[o]) {
            a = scan_argument(o, idx+1)
        } else a = 1
        set_long_option(o, a)
    } else if (match(a, /^-(.+)/, m)) {
        # short options
        split(m[1], m, "")
        for (i in m) { o = m[i]
            if (!(o in short_options)) {
                unknown_option_error(o)
                continue
            }
            o = short_options[o]
            if (long_options[o]) {
                if (i < length(m))
                    argument_required_error(m[i])
                else
                    a = scan_argument(m[i], idx+1)
            } else a = 1
            set_long_option(o, a)
        }
    } else
        return false
    return true
}

function scan_argument(o, i,    a) {
    if (!(i in ARGV) || ARGV[i] ~ /^-/)
        argument_required_error(o)
    else {
        a = ARGV[i]
        delete ARGV[i]
        if (a ~ /^(false|no)$/) a = 0
        return a
}   }

function set_long_option(o, a) {
    if (o in long_options) options[o] = a
    else unknown_option_error(o)
}

function unknown_option_error(o) {
    option_errors++
    print "! Unknown option: " o >"/dev/stderr"
}
function argument_required_error(o) {
    option_errors++
    print "! Argument required for option: " o >"/dev/stderr"
}

function may_show_help() {
    if (options["help"]) {
        print help_text
        exit 0
}   }

function provide_help_option() {
    if (!help_text) construct_help_text()
    if (!("help" in long_options))
        add_option("help", "h" in short_options ? 0 : "h")
}

function construct_help_text(    i, c, fmt, a, d) {
    if (!usage) usage = "<options>"
    help_text = "Usage: " ARGV[0] " " usage "\n"
    if (option_maxlength > max_option_maxlength)
        option_maxlength = max_option_maxlength
    c = PROCINFO["sorted_in"]
    PROCINFO["sorted_in"] = "@ind_num_asc"
    for (i in option_descriptions) {
        a = option_descriptions[i][1]
        if (length(a) < option_maxlength)
            fmt = "  %s\n  % "option_maxlength"s  %s"
        else
            fmt = "  %- "option_maxlength"s  %s%s"
        d = option_descriptions[i][2]
        help_text += sprintf(fmt, a, "", d)
    }
    if (c) PROCINFO["sorted_in"] = c
    else delete PROCINFO["sorted_in"]
}


