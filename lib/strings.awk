
# joining arrays of strings
function join(arr, sep,    i, r) {
    # only joins unbroken numerical array part
    # arr can start at either 0 or 1
    r = "" arr[0 in arr ? i+0 : ++i]
    while (++i in arr) r = r sep arr[i]
    return r }

# leaves out falsy elements
function skipjoin(arr, sep,    i, r) {
    # only joins unbroken numerical array part
    # arr can start at either 0 or 1
    r = "" arr[0 in arr ? i+0 : ++i]
    while (++i in arr) if(arr[i]) r = r sep arr[i]
    return r }

# undoes split() or patsplit()
function zipjoin(arr, seps,    i, r) {
    r = "" seps[i+0]
    while(++i in arr || i in seps)
        r = r arr[i] seps[i]
    return r }

# for constructing regex patterns out of strings
function regex_escape(str) {
    gsub(/[][(){}*+?|.^$\\]/, "\\\\&", str)
    return str }

