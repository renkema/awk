
# elements are indices
function element(set, elem) { set[elem] = 1 }

# these generate new sets
function union(r, a, b,          i) { delete r
    for (i in b) r[i] = b[i]
    for (i in a) r[i] = a[i] }
function intersection(r, a, b,   i) { delete r
    for (i in a) if (i in b) r[i] = a[i] }
function exclusion(r, a, b,      i) { delete r
    for (i in a) if (!(i in b)) r[i] = a[i] }

# and these manipulate the left set
function unite(a, b,            i) {
    for (i in b) a[i] = b[i] }
function intersect(a, b,        i) {
    for (i in a) if (!(i in b)) delete a[i] }
function exclude(a, b,          i) {
    for (i in a) if (i in b) delete a[i] }

