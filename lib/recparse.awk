
# Usage
#
#   @include "recparse"
#
# Variables
#
#   RECTYPE
#      Contains the record type.
#
#   RECF
#      Maps field names to field contents.
#
#   RECFNR
#      Maps field names to line numbers.
#
#   REC_FIELD_ORDER
#      Determines field order of recfmt().
#      Maps field names to numbers.
#
# Functions
#
#   recfmt(arr)
#      Format the array arr as record.
#      The argument is optional and defaults to RECF.
#
#   set_rec_field_order("space separated string")
#      Sets a field order for recfmt().
#
#   recparse::fail(message, fieldname)
#      Prints a message to stderr, with line number.
#
#   recparse::init()
#      Manually force recfile parsing (e.g. on stdin).

@namespace "recparse"

# initialisation and cleanup

BEGINFILE {
    if (FILENAME ~ /[.]rec$/) init()
}

function init() {
    RECTYPE = "default"
    _RECLINENR = 0
    _RECFSBK = FS; FS = "\n"
    _RECRSBK = RS; RS = "\n\n+"
}

ENDFILE {
    if (RECTYPE) {
        RECTYPE = _RECLINENR = 0
        delete RECF   # stores field values
        delete RECFNR # stores field line numbers
        FS = _RECFSBK
        RS = _RECRSBK
    }
}

# RECTYPE stores the record type

RECTYPE && /(^|\n)%rec:/ {
    RECTYPE = awk::gensub(/(^|\n)%rec:[ \t]?/,  "", 1, $1)
}

# parsing records

BEGIN {
    _RECFRE = @/^(%?[a-zA-Z0-9_]+):[ \t]?(.*)/
}

RECTYPE {
    delete RECF
    delete RECFNR
    _RECLASTF = 0
    for (i=1; i<=NF; i++) {
        _RECLINENR++
        if ($i ~ /^(#|$)/) {
            # do nothing
        } else if ($i ~ /^+/) {
            if (!_RECLASTF) fail("Record starts with line continuation")
            else RECF[_RECLASTF] == RECF[_RECLASTF] "\n" awk::gensub(/^+ ?/, "", 1, f) 
        } else if ($i ~ _RECFRE) {
            _RECLASTF = awk::gensub(_RECFRE, "\\1", 1, $i)
            RECF[_RECLASTF] = awk::gensub(_RECFRE, "\\2", 1, $i)
            RECFNR[_RECLASTF] = _RECLINENR
        } else fail("Illegal record field (skipped)")
    }
    _RECLINENR += count_newlines(RT) - 1 # last field has no \n
}

# adjusting line numbers

function count_newlines(str,    a) {
    split(str, a, "\n")
    return length(a) - 1
}

# empty record at the beginning of the document
RECTYPE && /^$/ { _RECLINENR++ }

# paragraphs with no fields
RECTYPE && !/(^|\n)[^#]/ { FNR--; NR--; next }

# helper functions

function fail(msg, field) {
    printf "! %s:%d %s\n", FILENAME, (field ? RECFNR[field] : _RECLINENR), msg >"/dev/stderr"
}

function order_fields(k1, v1, k2, v2,    o1, o2) {
    o1 = awk::REC_FIELD_ORDER[k1]
    o2 = awk::REC_FIELD_ORDER[k2]
    if(!o1) return o2 ?  1 : 0
    if(!o2) return o1 ? -1 : 0
    return o1<o2 ? -1 : o1>o2 ? 1 : 0
}

@namespace "awk"

# public functions

function set_rec_field_order(str,   a, i) {
    split(str, a, " ")
    for (i in a) REC_FIELD_ORDER[a[i]] = i
}

function recfmt(arr,    a, s, k, v, rv) {
    if (length(arr)==0) return recfmt(RECF)
    s = PROCINFO["sorted_in"]
    PROCINFO["sorted_in"] = "recparse::order_fields"
    for (k in arr) {
        v = awk::gensub(/\n/, "\n+ ", "g", arr[k])
        rv = rv k ": " v "\n"
    }
    PROCINFO["sorted_in"] = s
    return rv
}


