
@include "numbers" # for the min3() function

# calculates the levenshtein distance between two strings

function levenshtein(a, b,
        ca, cb, la, lb,
        m, n, i, j,
        sbs, del, ins) {
    # edge cases
    la = length(a); lb=length(b)
    if (!la) return lb
    if (!lb) return la
    if (la == lb && a==b) return 0
    # initialise starting vectors
    split(a, ca, "")
    split(b, cb, "")
    for (i=0;i<=la;i++) m[i] = i
    # fill matrix, only remembering the last two rows
    for (i=1;i<=lb;i++) {
        n = m[0]; m[0] = i
        for (j=1;j<=la;j++) {
            sbs = n + (ca[j]!=cb[i])
            ins = m[j] + 1
            del = m[j-1] + 1
            n = m[j]
            m[j] = min3(sbs, ins, del)
        }
    }; return m[la]
}
